class Interfaz {

	constructor() {
		this.init();
	}

	init() {
		this.construirSelect();
	}

	construirSelect () {
		apiCC.obtenerMonedasAPI()
			.then(monedas => {
				// Selecciona el select del index.html
				const select = document.querySelector('#criptomoneda');
				// Itera los resultados de la API
				for ( const [key, value] of Object.entries(monedas.monedas.Data) ) {
					// Leer el symbol y el nombre como pciones del select
					const opcion = document.createElement('option');
					opcion.value = value.Symbol;
					opcion.appendChild(document.createTextNode(value.CoinName));
					select.appendChild(opcion);
				}
			})
	}

	mostrarMensaje (mensaje, clases) {
		const div = document.createElement('div');
		div.className = clases;
		div.appendChild(document.createTextNode(mensaje));

		// Selecciona el div en donde se mostrarán los mensajes
		const divMensajes = document.querySelector('.mensajes');
		// Se inserta el elemento creado con su contenido
		divMensajes.appendChild(div);
		// Se retira el mensaje
		setTimeout( () =>{
			document.querySelector('.mensajes div').remove();
		}, 3000);
	}

	// Imprime el resultado de la conversion de las monedas
	mostrarResultado(resultado, moneda, cryptomoneda) {

		// Ocultar Resultado anterior
		const resultadoAnterior = document.querySelector('#resultado > div');
		if (resultadoAnterior) {
			resultadoAnterior.remove();
		}

		// Se envia la 'cryptomoneda' como llave para filtrar mejor los resultados
		// Tambien se envia el valor de la 'moneda' para extraer los valores de misma
		const datosMoneda = resultado[cryptomoneda][moneda];

		// Recortar digitos de precio y porcentaje
		let precio = datosMoneda.PRICE.toFixed(2),
			porcentaje = datosMoneda.CHANGEPCTDAY.toFixed(2);
		// Tranforma el timestamp de unix
		let actualizado = new Date (datosMoneda.LASTUPDATE * 1000).toLocaleDateString('es-MX');

		// Construir el template
		let templateHTML = `
				<div class="card bg-info">
					<div class="card-body text-light">
						<h2 class="card-title">Resultado:</h2>
						<p>El Precio de ${datosMoneda.FROMSYMBOL} a moneda ${datosMoneda.TOSYMBOL} es de: $${precio}</p>
						<p>Variación último día: % ${porcentaje}</p>
						<p>Ùltima actualizacion: ${actualizado}</p>
					</div>
				</div>
		`;

		// Mostrar Spinner
		this.mostrarOcultarSpinner('block');

		setTimeout( () => {
			// Insertar Resultado en el HTML despues de 2.5 segundos
			document.querySelector('#resultado').innerHTML = templateHTML;
			// Ocultar Spinner
			this.mostrarOcultarSpinner('none');
		},2500);
	}

	// Mostrar Spinner de carga al enviar la cotización
	mostrarOcultarSpinner (vista) {
		const spinner = document.querySelector('.contenido-spinner');
		spinner.style.display = vista;
	}

}