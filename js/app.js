// Instanciar Clases de UI y de API
// ApiKey de CryptoCompare -> https://www.cryptocompare.com/
const apiCC = new API(''); // <- Tu ApiKey
const ui = new Interfaz();

// Leer Campos
const formulario = document.querySelector('#formulario');

// EventListener
formulario.addEventListener('submit', (e) => {
	e.preventDefault();

	// Leer moneda seleccionada
	const monedas = document.querySelector('#moneda');
	const monedaSL = monedas.options[monedas.selectedIndex].value;

	// Leer criptomoneda seleccionada
	const cryptoMonedas = document.querySelector('#criptomoneda');
	const cryptoMonedaSL = cryptoMonedas.options[cryptoMonedas.selectedIndex].value;

	// Comprobar seleccion de monedas
	if (monedaSL === '' || cryptoMonedaSL === '') {
		ui.mostrarMensaje('Ambos campos son Obligatorios', 'alert bg-danger text-center');
	} else {
		apiCC.obtenerValores(monedaSL, cryptoMonedaSL)
			.then(data =>{
				ui.mostrarResultado(data.resultado.RAW, monedaSL, cryptoMonedaSL);
			})
	}
});