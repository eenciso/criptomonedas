
## Cotizador de Criptomonedas


Aplicacion Web creada para cotizar criptomonedas con Fecth API y Async Await, consumiendo la REST API de [CryptoCompare.com](https://www.cryptocompare.com)


#### Requisitos:
 - Api Key de [CryptoCompare.com](https://www.cryptocompare.com)

#### Instrucciones:
 - Generar una Api Key
 - Colocar el Api Key al instanciar la Clase API en el archivo [***app.js***](https://gitlab.com/eenciso/criptomonedas/blob/master/js/app.js).